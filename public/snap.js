function snapCrackle(maxValue) {
    let texto = "";

    for (let i = 1; i <= maxValue; i++) {

        if (i % 2 !== 0 && i % 5 === 0) {
            texto += "SnapCrackle, ";
        } else if (i % 2 !== 0) {
            texto += "Snap, ";
        } else if (i % 5 === 0) {
            texto += "Crackle, ";
        } else {
            texto += i + ", ";
        }
    }

    return texto;
}


function snapCracklePrime(maxValue) {
    let texto = "";
    
    for (let i = 1; i <= maxValue; i++) {

        if (verificaPrimo(i)) {
            texto += "Prime, ";
        } else if (i % 2 !== 0 && i % 5 === 0) {
            texto += "SnapCrackle, ";
        } else if (i % 2 !== 0) {
            texto += "Snap, ";
        } else if (i % 5 === 0) {
            texto += "Crackle, ";
        } else {
            texto += i + ", ";
        }
    }

    return texto;
}


function verificaPrimo(n) {
    if (n < 2) {
        return false;
    }

    let divisoes = 0;

    for (let i = 1; i <= n; i++) {
        if (n % i === 0) {
            divisoes++;
        }

        if (divisoes > 2) {
            return false;
        }
    }

    return true;
}


let resultadoSnapCracle = snapCrackle(12);
console.log(resultadoSnapCracle);

let resultadoSnapCracklePrime = snapCracklePrime(12);
console.log(resultadoSnapCracklePrime);
